Handlebars.registerHelper('lengthOf', function(arr) {
  if (arr)
    return arr.length
  else return 0
});