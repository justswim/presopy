getRandElt = function (myArray) {
  return myArray[Math.floor(Math.random() * myArray.length)];
}

contains = function (array, value) {
  return $.inArray(value, array) > -1
}

containsPerson = function (array, value) {
  var len = array.length
  for (var i = 0; i < len; i++) {
    if (array[i].name === value.name && array[i].email === value.email)
      return true
  }
}

getCurrentUserEmail = function () {
  if (Meteor.user() && Meteor.user().services && Meteor.user().services.google)
    return Meteor.user().services.google.email
}

replaceEmpty = function (str) {
  if (str === '')
    return undefined
  else return str
}

removeSpaces = function (str) {
  return str.replace(/ /g,'');
}

replaceURLWithHTMLLinks = function (text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
}

var entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
};

escapeHtml = function (string) {
  return String(string).replace(/[&<>"']/g, function (s) {
    return entityMap[s];
  });
}

showError = function (message) {
  var text = '<span>'+ message + '</span>';
  $(".bar-error").children("span").remove();
  $('.bar-error').append(text);
  $('.content').addClass('has-loading')
  $('.bar-error').show()

  Meteor.setTimeout(function () {
    $('.content').removeClass('has-loading')
    $('.bar-error').hide()
  }, 3000)
}

showSuccess = function (message) {
  var text = '<span>'+ message + '</span>';
  $(".bar-success").children("span").remove();
  $('.bar-success').append(text);
  $('.content').addClass('has-loading')
  $('.bar-success').show()

  Meteor.setTimeout(function () {
    $('.content').removeClass('has-loading')
    $('.bar-success').hide()
  }, 3000)
}

showWarning = function (message) {
  var text = '<span>'+ message + '</span>';
  $(".bar-warning").children("span").remove();
  $('.bar-warning').append(text);
  $('.content').addClass('has-loading')
  $('.bar-warning').show()

  Meteor.setTimeout(function () {
    $('.content').removeClass('has-loading')
    $('.bar-warning').hide()
  }, 3000)
}


getRandomInt = function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

getRandomProfPic = function getRandomProfPic () {
  var randInt = getRandomInt(1, 10)
  return '/samples/'+String(randInt)+'.jpeg'
}

getRandomCover = function getRandomCover () {
  var randInt = getRandomInt(1, 20)
  return '/covers/'+String(randInt)+'.jpeg'
}

getProfilePic = function getProfilePic (id) {
  return 'http://graph.facebook.com/' + id + '/picture?type=square'
}


getPhoto = function getPhoto (photo) {
  if (photo)
    return photo + "?access_token=" + Meteor.user().services.google.accessToken
  else return '/images/avatar.png'
}


validateEmail = function validateEmail(email) {
  email = email.replace(/\s+/g, '');
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

toTitleCase = function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

getUsername = function (str) {
  return str.replace(/@.*$/,"")
}

getEmail = function getEmail (userId) {
  return Meteor.users.findOne(userId).services.google.email
}

getUser = function getUser (email) {
  return Meteor.users.findOne({'services.google.email': email})
}

userEmail = function userEmail () {
  if (Meteor.user()
    && Meteor.user().services
    && Meteor.user().services.google)
  return Meteor.user().services.google.email
}

imgError = function imgError(image) {
    image.onerror = "";
    image.src = "/images/avatar.png";
    return true;
}

stripQuestion = function (s) {
  var n = s.indexOf('?');
  s = s.substring(0, n != -1 ? n : s.length);
  return s
}
