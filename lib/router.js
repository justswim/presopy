Router.configure({
  notFoundTemplate: 'notFound',
  waitOn: function () {
    return [Meteor.subscribe('sessions'),
      Meteor.subscribe('questions'),
      Meteor.subscribe('answers'),
      Meteor.subscribe('reactions')]
  },
})

HomeController = RouteController.extend({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  fastRender: true,
  action: function () {
    this.render()
  },
});

Router.route('/', {
  name: 'home',
  action: function () {
    this.render()
  },
  controller: 'HomeController',
})
