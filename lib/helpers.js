/* Helper functions for both client and server */

stripNum = function (number) {
  number = number.toString()
  number = number.replace(/\D/g,'');
  // add american country code
  if (number.length == 10)
    number = '1' + number
  if (number.length != 11)
    throw new Meteor.Error('invalid-number', 'An invalid phone number was entered.')
  return number
}

contains = function contains(a, obj) {
  var i = a.length;
  while (i--) {
     if (a[i] === obj) {
         return true;
     }
  }
  return false;
}