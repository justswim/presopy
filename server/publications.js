Meteor.publish('questions', function() {
  return Questions.find({})
})

Meteor.publish('sessions', function() {
  return Sessions.find({})
})

Meteor.publish('answers', function() {
  return Answers.find({})
})

Meteor.publish('reactions', function() {
  return Reactions.find({})
})
