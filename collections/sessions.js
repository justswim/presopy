Sessions = new Mongo.Collection("sessions");

var Schemas = {};

Schemas.Sessions = new SimpleSchema({
    name: {
      type: String,
      label: "Name of session",
    },
    code: {
      type: String,
      label: "Code for joining"
    },
    adminCode: {
      type: String,
      label: "Admin Code for joining",
    },
    isOpen: {
      type: Boolean,
      label: "Open session bit",
    },
    createdAt: {
      type: Date,
      label: "Created at timestamp",
    },
});

Sessions.attachSchema(Schemas.Sessions);
