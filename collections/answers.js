Answers = new Mongo.Collection("answers");

var Schemas = {};

Schemas.Answers = new SimpleSchema({
    sessionId: {
      type: String,
      label: "Session ID",
    },
    questionId: {
      type: String,
      label: "Question ID",
    },
    body: {
      type: String,
      label: "Answer text body",
    },
    isAdmin: {
      type: Boolean,
      label: "Student or admin bit",
    },
    createdAt: {
      type: Date,
      label: "Creation time",
    },
    deleted: {
      type: Boolean,
      label: "Deleted flag"
    },
});

Answers.attachSchema(Schemas.Answers);
