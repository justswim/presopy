Reactions = new Meteor.Collection("reactions")

var Schemas = {};

Schemas.Reactions = new SimpleSchema({
  sessionId: {
    type: String,
    label: "Session ID",
  },
  createdAt: {
    type: Date,
    label: "Created at timestamp",
  },
  reactionType: {
    type: Number,
    label: "Reaction type"
  },
})

Reactions.attachSchema(Schemas.Reactions);
